require 'rails_helper'

RSpec.describe Waypoint, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of(:latitude) }
    it { is_expected.to validate_presence_of(:longitude) }
    it { is_expected.to validate_presence_of(:sent_at) }
  end

  describe 'relations' do
    it { is_expected.to belong_to(:vehicle) }
  end
end
