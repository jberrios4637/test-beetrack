require 'rails_helper'

RSpec.describe Vehicle, type: :model do
  describe 'validations' do
    it { is_expected.to validate_presence_of(:vehicle_identifier) }
  end

  describe 'relations' do
    it { is_expected.to have_many(:waypoints).dependent(:destroy) }
  end

  context 'when call last_location' do
    let(:sent_at) { Time.zone.now }

    before do
      v1 = create(:vehicle, vehicle_identifier: 'H30-PE')
      create(:waypoint, vehicle: v1, sent_at: '2020-10-09 00:00:00')
      create(:waypoint, vehicle: v1, sent_at: '2020-10-12 13:00:00')
      create(:waypoint, vehicle: v1, sent_at: sent_at)
      v2 = create(:vehicle, vehicle_identifier: 'H31-PE')
      create(:waypoint, vehicle: v2, sent_at: '2020-10-02 14:00:00')
      create(:waypoint, vehicle: v2, sent_at: '2020-10-11 22:00:00')
      create(:waypoint, vehicle: v2, sent_at: sent_at)
      v3 = create(:vehicle, vehicle_identifier: 'H32-PE')
      create(:waypoint, vehicle: v3, sent_at: '2020-10-02 11:45:00')
      create(:waypoint, vehicle: v3, sent_at: '2020-09-29 23:59:00')
      create(:waypoint, vehicle: v3, sent_at: sent_at)
    end

    it 'return correct location' do
      expect(described_class.last_waypoints.map { |l| l[:sent_at] }.uniq).to eq [sent_at]
    end
  end
end
