require 'rails_helper'

RSpec.describe 'Waypoints', type: :request do
  context 'when there is an invalidate params' do
    let(:params) do
      {
        latitude: 'xxx',
        longitude: 'xxx',
        sent_at: Time.zone.now
      }
    end

    before do
      post '/api/v1/gps', params: params
    end

    it 'return error' do
      expect(response).to have_http_status(:bad_request)
    end
  end

  context 'when all params are valid' do
    let(:params) do
      {
        vehicle_identifier: vehicle_identifier,
        latitude: latitude,
        longitude: longitude,
        sent_at: Time.zone.now
      }
    end

    context 'when the vehicle exist in DB' do
      let(:vehicle_identifier) { 'THD-656' }
      let(:latitude) { Faker::Address.latitude }
      let(:longitude) { Faker::Address.longitude }

      before do
        create(:vehicle, vehicle_identifier: vehicle_identifier)
        post '/api/v1/gps', params: params
      end

      it 'return code 200' do
        expect(response).to have_http_status(:ok)
      end

      it 'return correct longitude' do
        expect(response.parsed_body['data']['longitude']).to eq longitude.to_s
      end

      it 'return correct latitude' do
        expect(response.parsed_body['data']['latitude']).to eq latitude.to_s
      end
    end

    context 'when the vehicle not exist in DB' do
      let(:vehicle_identifier) { 'P10-SKX' }
      let(:latitude) { Faker::Address.latitude }
      let(:longitude) { Faker::Address.longitude }

      before do
        post '/api/v1/gps', params: params
      end

      it 'return code 200' do
        expect(response).to have_http_status(:ok)
      end

      it 'create a vehicle' do
        expect(Vehicle.find_by(vehicle_identifier: vehicle_identifier).present?).to eq true
      end

      it 'return correct longitude' do
        expect(response.parsed_body['data']['longitude']).to eq longitude.to_s
      end

      it 'return correct latitude' do
        expect(response.parsed_body['data']['latitude']).to eq latitude.to_s
      end
    end
  end
end
