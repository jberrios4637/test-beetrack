require 'rails_helper'

RSpec.describe 'Waypoints', type: :request do
  before do
    get '/show', params: {}
  end

  it 'return code 200' do
    expect(response).to have_http_status(:ok)
  end
end
