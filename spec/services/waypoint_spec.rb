require 'rails_helper'

RSpec.describe Services::Waypoint do
  subject(:service) { described_class.new }

  let(:vehicle_identifier) { 'HT-609' }
  let(:latitude) { '-31.56391' }
  let(:longitude) { '147.154312' }
  let(:sent_at) { Time.zone.now }

  context 'when vehicle not exist' do
    before do
      service.call!(vehicle_identifier, latitude, longitude, sent_at)
    end

    it 'create a vehicle' do
      expect(Vehicle.count).to eq 1
    end

    it 'create a location' do
      expect(Waypoint.count).to eq 1
    end
  end

  context 'when vehicle exist' do
    before do
      create(:vehicle, :create_waypoints, vehicle_identifier: vehicle_identifier)
      service.call!(vehicle_identifier, latitude, longitude, sent_at)
    end

    it 'increase locations in one' do
      expect(Waypoint.count).to eq 4
    end
  end
end
