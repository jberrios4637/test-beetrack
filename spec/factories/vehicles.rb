FactoryBot.define do
  factory :vehicle do
    vehicle_identifier { Faker::Alphanumeric.alpha(number: 5) }

    trait :create_waypoints do
      after(:create) do |vehicle|
        2.downto(0).each { |_| create(:waypoint, vehicle: vehicle) }
      end
    end
  end
end
