FactoryBot.define do
  factory :waypoint do
    latitude { Faker::Address.latitude }
    longitude { Faker::Address.longitude }
    sent_at { Faker::Date.in_date_period }
    vehicle
  end
end
