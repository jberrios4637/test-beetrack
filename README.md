  ### Base de datos
  * Usa por deafult sqlite3, no necesita ninguna configuracion

  ### Comandos
  Instalar las gemas
  1. `gem install bundler`
  2. `bundle install`

  Crear y migrar la base de datos para rspec
  1. `bundle exec rake db:create RAILS_ENV=test`
  2. `bundle exec rake db:migrate RAILS_ENV=test`

  Crear y migrar la base de datos para entorno de pruebas locales
  1. `bundle exec rake db:create RAILS_ENV=development`
  2. `bundle exec rake db:migrate RAILS_ENV=development`
  3. `bundle exec rake db:seed RAILS_ENV=development`
  
  Comandos utiles
  * `bundle exec rails s`, iniciar el server
  * `bundle exec rails c`, iniciar una consola
  * `bundle exec rspec`, ejecuta los test
  
  ### Endpoints
  
  * `/show` retorna un html con las ultimas posiciones en un mapa
    * method: get
   
  * `/api/v1/gps` permite guardar un waypoint
    * method: post
    * params: 
    ```
    {
        "latitude": 20.23,
        "longitude": -0.56,
        "sent_at": "2020-10-16 20:45:00",
        "vehicle_identifier": "ACK-E290"
      }
    ```