module Services
  class Waypoint
    def call!(vehicle_identifier, latitude, longitude, sent_at)
      vehicle = Vehicle.find_or_create_by!(vehicle_identifier: vehicle_identifier)
      waypoint = ::Waypoint.new(
        latitude: latitude, longitude: longitude, sent_at: sent_at
      )
      vehicle.waypoints << waypoint
      waypoint
    end
  end
end
