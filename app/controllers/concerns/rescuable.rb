module Rescuable
  def render_errors(message, code, status)
    render json: { errors: [{ message: message, code: code }] }, status: status
  end

  def self.included(base)
    base.rescue_from(ActionController::ParameterMissing) do
      render_errors('The params are not complete', 400, 400)
    end
  end
end
