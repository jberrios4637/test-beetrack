class WaypointsController < ApplicationController
  def show
    @last_waypoints = Vehicle.last_waypoints
    render template: 'waypoints/show'
  end
end
