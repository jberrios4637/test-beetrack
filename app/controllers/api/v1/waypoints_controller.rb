module Api
  module V1
    class WaypointsController < ApplicationController
      before_action :validate_params

      def create
        params = params_permit
        result = Services::Waypoint.new.call!(
          params['vehicle_identifier'], params['latitude'], params['longitude'], params['sent_at']
        )
        render json: { data: result }, status: :ok
      end

      private

      def validate_params
        params.require(:vehicle_identifier)
        params.require(:latitude)
        params.require(:longitude)
        params.require(:sent_at)
      end

      def params_permit
        params.permit(:vehicle_identifier, :latitude, :longitude, :sent_at)
      end
    end
  end
end
