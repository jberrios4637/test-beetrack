class Vehicle < ApplicationRecord
  validates :vehicle_identifier, presence: true
  has_many :waypoints, dependent: :destroy

  scope :with_waypoints, -> { includes(:waypoints) }

  class << self
    def last_waypoints
      with_waypoints.map do |vehicle|
        last_waypoint = vehicle.waypoints.order('sent_at DESC').take
        {
          vehicle_identifier: vehicle.vehicle_identifier,
          latitude: last_waypoint.latitude,
          longitude: last_waypoint.longitude,
          sent_at: last_waypoint.sent_at
        }
      end
    end
  end
end
