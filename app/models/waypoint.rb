class Waypoint < ApplicationRecord
  validates :latitude, :longitude, :sent_at, presence: true
  belongs_to :vehicle
end
