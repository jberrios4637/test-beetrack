vehicle = Vehicle.create!(vehicle_identifier: 'HTA-XYZ')
vehicle.waypoints << Waypoint.new(latitude: '-34.6415136', longitude: '-58.4603377', sent_at: Time.zone.now - 5.hours)
vehicle.waypoints << Waypoint.new(latitude: '-34.6379789', longitude: '-58.4645182', sent_at: Time.zone.now - 4.hours)
vehicle.waypoints << Waypoint.new(latitude: '-34.6335299', longitude: '-58.468744', sent_at: Time.zone.now - 3.hours)

vehicle = Vehicle.create!(vehicle_identifier: 'PDE-O91')
vehicle.waypoints << Waypoint.new(latitude: '-34.5632976', longitude: '-58.5368507', sent_at: Time.zone.now - 6.hours)
vehicle.waypoints << Waypoint.new(latitude: '-34.5072236', longitude: '-58.5873187', sent_at: Time.zone.now - 2.hours)
vehicle.waypoints << Waypoint.new(latitude: '-34.457238', longitude: '-58.611866', sent_at: Time.zone.now)


vehicle = Vehicle.create!(vehicle_identifier: 'ACK-E29O')
vehicle.waypoints << Waypoint.new(latitude: '-31.371884', longitude: '-64.222156', sent_at: Time.zone.now - 10.hours)
vehicle.waypoints << Waypoint.new(latitude: '-31.344394', longitude: '-64.225890', sent_at: Time.zone.now - 5.hours)
vehicle.waypoints << Waypoint.new(latitude: '-31.315576', longitude: '-64.217822', sent_at: Time.zone.now - 2.hours)