class RenameLocationToWaypoint < ActiveRecord::Migration[6.0]
  def change
    rename_table :locations, :waypoints
  end
end
