class CreateLocations < ActiveRecord::Migration[6.0]
  def change
    create_table :locations do |t|
      t.string :latitude
      t.string :longitude
      t.datetime :sent_at
      t.belongs_to :vehicle

      t.timestamps
    end
  end
end
