class AddIndexUniqToVehicle < ActiveRecord::Migration[6.0]
  def change
    add_index(:vehicles, :vehicle_identifier, unique: true, name: 'index_vehicle_identifier')
  end
end
